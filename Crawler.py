from bs4 import BeautifulSoup
import requests
import sqlite3

ERROR_CODE = 333112

def web_crawler(url, database):
	print('WRITING: ' + database)

	conn = sqlite3.connect('Vaktin.sqlite')
	conn.text_factory = str

	source_code = requests.get(url)
	soup = BeautifulSoup(source_code.text , "html.parser")
	
	tableData = soup.findAll('td')

	num = 0
	c = conn.cursor()

	while num < int(len(tableData)):
		databaseArray = (
			tableData[num].text,
			createInteger(tableData[num + 1].text),
			createInteger(tableData[num + 2].text),
			createInteger(tableData[num + 3].text),
			createInteger(tableData[num + 4].text),
			createInteger(tableData[num + 5].text),
			createInteger(tableData[num + 6].text),
			createInteger(tableData[num + 7].text),
			createInteger(tableData[num + 8].text)
		)
		try:
			if database == 'PROCESSOR':
				c.execute('INSERT INTO PROCESSOR VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', databaseArray)
			elif database == 'MEMORY':
				c.execute('INSERT INTO MEMORY VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', databaseArray)
			elif database == 'GPU':
				c.execute('INSERT INTO GPU VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', databaseArray)
			elif database == 'HDD':
				c.execute('INSERT INTO HDD VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', databaseArray)
			elif database == 'MOTHERBOARD':
				c.execute('INSERT INTO MOTHERBOARD VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', databaseArray)
			elif database == 'POWER':
				c.execute('INSERT INTO POWER VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', databaseArray)
			elif database == 'COMPUTER_CASE':
				c.execute('INSERT INTO COMPUTER_CASE VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', databaseArray)
			elif database == 'MOUSE':
				c.execute('INSERT INTO MOUSE VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', databaseArray)

			conn.commit()
		except sqlite3.Error, e:
			if conn:
				conn.rollback()

			print 'Error %s:' % e.args[0]

		print(databaseArray[0] + ': ' + str(databaseArray[1]) + ' ' + str(databaseArray[2]) + ' ' + str(databaseArray[3]) + ' ' + str(databaseArray[4]) + ' ' + str(databaseArray[5]) + ' ' + str(databaseArray[6]) + ' ' + str(databaseArray[7]) + ' ' + str(databaseArray[8]))

		num = num + 9
	
	if conn:
		conn.close()

	print('\n\n')

def createInteger(number):
	splitted = number.split('.')

	returnValue = ''.join(splitted)

	if returnValue.isspace() or returnValue is '':
		return ERROR_CODE

	return int(returnValue)

web_crawler('http://www.vaktin.is/index.php?action=prices&method=display&cid=11', 'PROCESSOR')
web_crawler('http://www.vaktin.is/index.php?action=prices&method=display&cid=14', 'MEMORY')
web_crawler('http://www.vaktin.is/index.php?action=prices&method=display&cid=12', 'GPU')
web_crawler('http://www.vaktin.is/index.php?action=prices&method=display&cid=10', 'HDD')
web_crawler('http://www.vaktin.is/index.php?action=prices&method=display&cid=16', 'MOTHERBOARD')
web_crawler('http://www.vaktin.is/index.php?action=prices&method=display&cid=18', 'POWER')
web_crawler('http://www.vaktin.is/index.php?action=prices&method=display&cid=21', 'COMPUTER_CASE')
web_crawler('http://www.vaktin.is/index.php?action=prices&method=display&cid=22', 'MOUSE')